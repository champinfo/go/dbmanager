//go:generate stringer -type=Pill

package main

import (
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
	"reflect"
	"runtime"
	"strings"
	"text/template"
	"time"
)

var (
	op string
	pkg string
)

const (
	c   string = "create"
	a   string = "alter"
	d   string = "delete"
	m   string = "migrate"
	goFile string = "%s_%s_%s.go"
	funcName string = "%s%s%s"
	migrationsFolder = "migrations"
	templatesFolder = "templates"
)

type Migration struct {
	Func string
	Table   interface{}
}

// 設定要從 command line 讀取的參數
// 這邊所設定的會在 -h 或者輸入錯誤時出現提示
func init()  {
	//使用方法 -c restaurants
	flag.StringVar(&op, "op", "c", "operate table migration")
	flag.StringVar(&pkg, "pkg", "", "table package")
	flag.Usage = usage
}

// 印出預設的說明
func usage()  {
	if _, err := fmt.Fprintf(os.Stderr, "Usage: migration -op=[operator] -pkg=[package] [database table]\n"); err != nil {

	}
	if _, err := fmt.Fprintf(os.Stderr, " There are three options: create -c, alter -a, delete -d, migrate -m\n"); err != nil {

	}
	flag.PrintDefaults()
}

func main() {
	root := getRootDir()

	// 將command line上的arguments進行處理
	flag.Parse()

	// 取得argument
	table := flag.Arg(0)

	//check migrations folder
	migrationsPath := root + "/" + migrationsFolder
	_, err := os.Stat(migrationsPath)
	if os.IsNotExist(err) {
		fmt.Printf("%s does not exist\n", migrationsFolder)
		//create the folder
		if err := os.MkdirAll(migrationsPath, os.ModePerm); err != nil {
			log.Fatalf("create %s failed!", migrationsPath)
		}
		fmt.Printf("create %s success\n", migrationsPath)
	}

	switch op {
	case "c": //create
		//建立migrations
		timestamp := getTimeStamp()
		fName := generateFuncName(timestamp, c, table)
		createPath := migrationsPath + "/" + generateGoFile(timestamp, c, strings.ToLower(table))

		tablePkg := pkg + "." + table + "{}"
		if err := createMigrationFromTemplate(createPath, fName, tablePkg); err != nil {
			log.Fatalf("create migration file %s failed", createPath)
		}
	case "a": //alter
		//更新migrations
		timestamp := getTimeStamp()
		fName := generateFuncName(timestamp, a, table)
		alterPath := migrationsPath + "/" + generateGoFile(timestamp, a, strings.ToLower(table))
		if err := createMigrationFromTemplate(alterPath, fName, table); err != nil {
			log.Fatalf("touch %s failed!", alterPath)
		}
	case "d": //delete
		//刪除migrations
		timestamp := getTimeStamp()
		fName := generateFuncName(timestamp, d, table)
		deletePath := migrationsPath + "/" + generateGoFile(timestamp, d, strings.ToLower(table))
		if err := createMigrationFromTemplate(deletePath, fName, table); err != nil {
			log.Fatalf("touch %s failed!", deletePath)
		}
	case "m":
		//execute migration
		files, err := listFiles(migrationsPath)
		if err != nil {
			log.Printf("%s is empty", migrationsPath)
		}
		for _, file := range files {
			//get the last element of path
			element := filepath.Base(file)
			fmt.Println(element)
			// check id in the migrations table
		}
	}
}

func getTimeStamp() string {
	now := time.Now()
	formatted := fmt.Sprintf("%d%02d%02d%02d%02d%02d", now.Year(), now.Month(), now.Day(), now.Hour(), now.Minute(), now.Second())
	return formatted
}

func listFiles(folder string) ([]string, error)  {
	var files []string
	if err := filepath.Walk(folder, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() == false {
			files = append(files, path)
		}
		return nil
	}); err != nil {
		return nil, errors.New(err.Error())
	}
	return files, nil
}

func touchMigrationFile(name string) error {
	file, err := os.OpenFile(name, os.O_RDONLY | os.O_CREATE, 0644)
	if err != nil {
		return err
	}
	return file.Close()
}

func writeMigrationFile(name string, body []byte) error {
	if err := ioutil.WriteFile(name, body, 0644); err != nil {
		return err
	}
	return nil
}

func createMigrationFromTemplate(filePath string, funName string, table string) error {
	//templates folder
	templatesPath := getRootDir() + "/" + templatesFolder + "/migration.tmpl"
	tpl, err := template.ParseFiles(templatesPath)
	if err != nil {
		log.Fatalln("can not open migration template file")
		return err
	}

	newFile, err := os.Create(filePath)

	defer newFile.Close()

	if err != nil {
		log.Fatalf("create %s failed!", filePath)
		return err
	}
	fmt.Printf("create %s success\n", filePath)
	if err := tpl.Execute(newFile, Migration{Func: funName, Table: table}); err != nil {
		log.Fatalf("can not generate template file %s", filePath)
		return err
	}

	return nil
}

func generateGoFile(timestamp string, operator string, name string) string {
	return fmt.Sprintf(goFile, timestamp, operator, name)
}

//func generateFileName(timestamp string, operator string, name string) string {
//	return fmt.Sprintf(fileName, timestamp, operator, name)
//}

func generateFuncName(timestamp string, operator string, name string) string {
	return fmt.Sprintf(funcName, strings.Title(strings.ToLower(operator)), strings.Title(strings.ToLower(name)), strings.Title(strings.ToLower(timestamp)))
}

//getRooDir Get the project root directory
func getRootDir() string {
	_, b, _, _ := runtime.Caller(0)
	dir := path.Join(path.Dir(b))
	return filepath.Dir(dir)
}

func GetFunctionName(i interface{}) string {
	return runtime.FuncForPC(reflect.ValueOf(i).Pointer()).Name()
}