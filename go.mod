module git.championtek.com.tw/go/dbmanager

go 1.14

require (
	github.com/dlmiddlecote/sqlstats v1.0.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/go-xorm/xorm v0.7.9
	github.com/prometheus/client_golang v1.5.1
)
