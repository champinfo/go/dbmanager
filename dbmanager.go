package databases

import (
	"database/sql"
	"errors"
	"fmt"
	"github.com/dlmiddlecote/sqlstats"
	_ "github.com/go-sql-driver/mysql"
	"github.com/go-xorm/xorm"
	"github.com/go-xorm/xorm/migrate"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"io"
	"log"
	"net"
	"net/http"
	"reflect"
	"time"
)

type DBManager interface {
	Init(config *Config) error
	GetSession() *xorm.Session
	Close() error
	Migrate(beans ...interface{}) error
	CreateTables(beans ...interface{}) error
	DropTables(beans ...interface{}) error
	IsRecordExist(bean interface{}) (bool, error)
	CreateRecords(beans ...interface{}) (int64, error)
	DeleteRecord(bean interface{}) (int64, error)
	GetRecord(bean interface{}) (bool, error)
	//Since we do not use autoincrement id, we can not use cursor based pagination
	FindRecords(bean interface{}, itemPerPage int, page int, order *Order, conditionBeans ...interface{}) error
	FindLikeRecords(bean interface{}, itemPerPage int, page int, order *Order, query interface{}, args ...interface{}) error
	FindJoinRecords(bean interface{}, table interface{}, itemPerPage int, page int, order *Order, joinOp string, joinTable interface{}, query string, args ...interface{}) error

	FindAndCount(beans interface{}, conditionBeans ...interface{}) (int64, error)
	UpdateRecord(bean interface{}, conditionBeans ...interface{}) (int64, error)

	//single db session transaction
	InsertRecordsTransaction(bean interface{}, cascades ...interface{}) (interface{}, error)
	DeleteRecordsTransaction(bean interface{}, cascades ...interface{}) (interface{}, error)
	UpdateRecordsTransaction(bean interface{}, conditionBean interface{}, conditionCascade interface{}, cascades ...interface{}) (interface{}, error)

	//multiple transaction
	InsertMultiDBRecordsTransaction(objs []InsertTransactionObj, fns ...insertFn) (interface{}, error)
	DeleteMultiDBRecordsTransaction(objs []DeleteTransactionObj, fns ...deleteFn) (interface{}, error)
	UpdateMultiDBRecordsTransaction(objs []UpdateTransactionObj, fns ...updateFn) (interface{}, error)

	ConvertRFC3339ToDate(datetime string) (sql.NullString, error)
}

type Config struct {
	Driver             string        `default:"mysql" yaml:"Driver"`
	UserName           string        `required:"true" yaml:"UserName"`
	Password           string        `required:"true" yaml:"Password"`
	Host               []string      `required:"true" yaml:"Host"`
	Port               []string        `required:"true" yaml:"Port"`
	DBName             string        `required:"true" yaml:"DBName"`
	IsDebug            bool          `default:"false" yaml:"IsDebug"`
	MaxIdleConnections int           `default:"2048" yaml:"MaxIdleConnections"`
	MaxOpenConnections int           `default:"1024" yaml:"MaxOpenConnections"`
	MaxLifeTime        time.Duration `default:"0" yaml:"MaxLifeTime"`
	MonitorStatusPort  int           `default:"9400" yaml:"MonitorStatusPort"`
}

type Order struct {
	Keyword string
	Desc bool
}

type dbmanager struct {
	db        *xorm.EngineGroup
	srvCloser io.Closer
}

var Mgr DBManager

func init() {
	Mgr = &dbmanager{}
}

func listenAndServeWithClose(addr string, handler http.Handler) (io.Closer, error) {
	var listener net.Listener

	srv := &http.Server{Addr: addr, Handler: handler}

	if addr == "" {
		addr = ":http"
	}

	listener, err := net.Listen("tcp", addr)
	if err != nil {
		return nil, err
	}

	go func() {
		err := srv.Serve(tcpKeepAliveListener{listener.(*net.TCPListener)})
		if err != nil {
			log.Println("HTTP Server Error - ", err)
		}
	}()

	return listener, nil
}

type tcpKeepAliveListener struct {
	*net.TCPListener
}

func (ln tcpKeepAliveListener) Accept() (c net.Conn, err error) {
	tc, err := ln.AcceptTCP()
	if err != nil {
		return
	}
	if err := tc.SetKeepAlive(true); err != nil {
		return nil, err
	}

	if err := tc.SetKeepAlivePeriod(3 * time.Minute); err != nil {
		return nil, err
	}
	return tc, nil
}

//NewDBManager 初始化資料庫設定
func (dm *dbmanager) Init(config *Config) error {
	if config == nil {
		log.Println("database config can not be nil")
		return errors.New("database config can not be nil")
	}

	dbSource := make([]string, len(config.Host))
	for i, h := range config.Host {
		dbSource[i] = fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", config.UserName, config.Password, h, config.Port[i], config.DBName)
		log.Println(dbSource[i])
	}
	
	db, err := xorm.NewEngineGroup(config.Driver, dbSource)
	if err != nil {
		errStr := fmt.Sprintf("initialize database failed: %v", err)
		log.Println(errStr)
		return errors.New(errStr)
	}

	if err = db.Ping(); err != nil {
		errStr := fmt.Sprintf("database is not alive: %#v", err)
		log.Println(errStr)
		return errors.New(errStr)
	}

	db.SetMaxIdleConns(config.MaxIdleConnections)
	db.SetMaxOpenConns(config.MaxOpenConnections)
	db.SetConnMaxLifetime(config.MaxLifeTime) //If d <= 0, connections are reused forever.

	if config.IsDebug {
		db.SetLogLevel(xorm.DEFAULT_LOG_LEVEL)
		db.ShowSQL(true)
		db.ShowExecTime(true)
	}

	dm.db = db

	if config.MonitorStatusPort != 0 {
		// create a new collector, the name will be used as a label on the metrics
		collector := sqlstats.NewStatsCollector(config.DBName, db.DB())
		// register it with Prometheus
		prometheus.MustRegister(collector)
		// register the metrics handler
		http.Handle("/metrics", promhttp.Handler())

		host := fmt.Sprintf(":%d", config.MonitorStatusPort)
		srvCloser, _ := listenAndServeWithClose(host, nil)

		dm.srvCloser = srvCloser
	}

	return err
}

func (dm *dbmanager) Migrate(beans ...interface{}) error {
	var migrations = make([]*migrate.Migration, 0, 0)

	var migration = migrate.Migration{
		ID: time.Now().Format("200612150405"),
		Migrate: func(engine *xorm.Engine) error {
			return engine.Sync2(beans...)
		},
		Rollback: func(engine *xorm.Engine) error {
			return engine.DropTables(beans...)
		},
	}

	migrations = append(migrations, &migration)

	m := migrate.New(dm.db.Master(), &migrate.Options{
		TableName:    "migrations",
		IDColumnName: "id",
	}, migrations)

	return m.Migrate()
}

func (dm *dbmanager) CreateTables(beans ...interface{}) error {
	return dm.db.CreateTables(beans...)
}

func (dm *dbmanager) DropTables(beans ...interface{}) error {
	return dm.db.DropTables(beans...)
}

//IsRecordExist check record exist or not
func (dm *dbmanager) IsRecordExist(bean interface{}) (bool, error) {
	return dm.db.Exist(bean)
}

// CreateRecords INSERT INTO structures () values ()
func (dm *dbmanager) CreateRecords(beans ...interface{}) (int64, error) {
	//for _, bean := range beans {
	//	switch bean.(type) {
	//	case map[string]string:
	//		for k := range bean.(map[string]sql.NullString) {
	//			bean.(map[string]sql.NullString)[k] = newNullString(bean.(map[string]string)[k])
	//		}
	//	case []map[string]string:
	//		s := bean.([]map[string]string)
	//		for i := 0; i < len(s); i++ {
	//			item := s[i]
	//			for k := range item {
	//				item.(map[string]string)[k] = newNullString(item.(map[string]string)[k])
	//			}
	//		}
	//	}
	//}
	return dm.db.Insert(beans...)
}

func (dm *dbmanager) DeleteRecord(bean interface{}) (int64, error) {
	return dm.db.Delete(bean)
}

// GetRecord SELECT * FROM $bean LIMIT 1
func (dm *dbmanager) GetRecord(bean interface{}) (bool, error) {
	return dm.db.Get(bean)
}

func (dm *dbmanager) GetSession() *xorm.Session {
	return dm.db.NewSession()
}

func (dm *dbmanager) FindLikeRecords(bean interface{}, itemPerPage int, page int, order *Order, query interface{}, args ...interface{}) error {
	if itemPerPage == 0 || page == 0 {
		if order != nil {
			if len(order.Keyword) > 0 {
				if order.Desc {
					return dm.db.Where(query, args...).Desc(order.Keyword).Find(bean)
				} else {
					return dm.db.Where(query, args...).Asc(order.Keyword).Find(bean)
				}
			} else {
				return errors.New("the column name for order can not be empty")
			}
		} else {
			return dm.db.Where(query, args...).Find(bean)
		}
	} else {
		var offset = (page - 1) * itemPerPage
		if order != nil {
			if len(order.Keyword) > 0 {
				if order.Desc {
					return dm.db.Limit(itemPerPage, offset).Where(query, args...).Desc(order.Keyword).Find(bean)
				} else {
					return dm.db.Limit(itemPerPage, offset).Where(query, args...).Asc(order.Keyword).Find(bean)
				}
			} else {
				return errors.New("the column name for order can not be empty")
			}
		} else {
			return dm.db.Limit(itemPerPage, offset).Where(query, args...).Find(bean)
		}
	}
}

func (dm *dbmanager) FindJoinRecords(bean interface{}, table interface{}, itemPerPage int, page int, order *Order, joinOp string, joinTable interface{}, query string, args ...interface{}) error {
	if itemPerPage == 0 || page == 0 {
		if order != nil {
			if len(order.Keyword) > 0 {
				if order.Desc {
					return dm.db.Table(table).Join(joinOp, joinTable, query, args...).Desc(order.Keyword).Find(bean)
				} else {
					return dm.db.Table(table).Join(joinOp, joinTable, query, args...).Asc(order.Keyword).Find(bean)
				}
			} else {
				return errors.New("the column name for order can not be empty")
			}
		} else {
			return dm.db.Table(table).Join(joinOp, joinTable, query, args...).Find(bean)
		}
	} else {
		var offset = (page - 1) * itemPerPage
		if order != nil {
			if len(order.Keyword) > 0 {
				if order.Desc {
					return dm.db.Table(table).Limit(itemPerPage, offset).Join(joinOp, joinTable, query, args...).Desc(order.Keyword).Find(bean)
				} else {
					return dm.db.Table(table).Limit(itemPerPage, offset).Join(joinOp, joinTable, query, args...).Asc(order.Keyword).Find(bean)
				}
			} else {
				return errors.New("the column name for order can not be empty")
			}
		} else {
			return dm.db.Table(table).Limit(itemPerPage, offset).Join(joinOp, joinTable, query, args...).Find(bean)
		}
	}
}

func (dm *dbmanager) FindRecords(bean interface{}, itemPerPage int, page int, order *Order, conditionBeans ...interface{}) error {
	if itemPerPage == 0 || page == 0 {
		if order != nil {
			if len(order.Keyword) > 0 {
				if order.Desc {
					return dm.db.Desc(order.Keyword).Find(bean, conditionBeans)
				} else {
					return dm.db.Asc(order.Keyword).Find(bean, conditionBeans)
				}
			} else {
				return errors.New("the column name for order can not be empty")
			}
		} else {
			return dm.db.Find(bean, conditionBeans...)
		}
	} else {
		var offset = (page - 1) * itemPerPage
		if order != nil {
			if len(order.Keyword) > 0 {
				if order.Desc {
					return dm.db.Limit(itemPerPage, offset).Desc(order.Keyword).Find(bean, conditionBeans...)
				} else {
					return dm.db.Limit(itemPerPage, offset).Asc(order.Keyword).Find(bean, conditionBeans...)
				}
			} else {
				return errors.New("the column name for order can not be empty")
			}
		} else  {
			return dm.db.Limit(itemPerPage, offset).Find(bean, conditionBeans...)
		}
	}
}

func (dm *dbmanager) FindAndCount(beans interface{}, conditionBeans ...interface{}) (int64, error) {
	return dm.db.FindAndCount(beans, conditionBeans...)
}

func (dm *dbmanager) UpdateRecord(bean interface{}, conditionBeans ...interface{}) (int64, error) {
	return dm.db.Update(bean, conditionBeans...)
}

func (dm *dbmanager) InsertRecordsTransaction(bean interface{}, cascades ...interface{}) (interface{}, error) {
	return dm.db.Transaction(func(session *xorm.Session) (interface{}, error) {
		if _, err := session.Insert(bean); err != nil {
			return nil, err
		}

		if _, err := session.Insert(cascades...); err != nil {
			return nil, err
		}

		return nil, nil
	})
}

func (dm *dbmanager) DeleteRecordsTransaction(bean interface{}, cascades ...interface{}) (interface{}, error) {
	return dm.db.Transaction(func(session *xorm.Session) (interface{}, error) {
		if _, err := session.Delete(bean); err != nil {
			return nil, err
		}

		for _, cascade := range cascades {
			if _, err := session.Delete(cascade); err != nil {
				return nil, err
			}
		}

		return nil, nil
	})
}

func (dm *dbmanager) UpdateRecordsTransaction(bean interface{}, conditionBean interface{}, conditionCascade interface{}, cascades ...interface{}) (interface{}, error) {
	return dm.db.Transaction(func(session *xorm.Session) (interface{}, error) {
		if _, err := session.Update(bean, conditionBean); err != nil {
			return nil, err
		}

		for _, cascade := range cascades {
			if _, err := session.Update(cascade, conditionCascade); err != nil {
				return nil, err
			}
		}

		return nil, nil
	})
}

type insertFn func(bean interface{}, cascades ...interface{}) (interface{}, error)
type deleteFn func(bean interface{}, cascades ...interface{}) (interface{}, error)
type updateFn func(bean interface{}, conditionBean interface{}, conditionCascade interface{}, cascades ...interface{}) (interface{}, error)

func (dm *dbmanager) InsertMultiDBRecordsTransaction(objs []InsertTransactionObj, fns ...insertFn) (interface{}, error) {
	//TODO - create reverse insertion manager
	return nil, nil
}

func (dm *dbmanager) DeleteMultiDBRecordsTransaction(objs []DeleteTransactionObj, fns ...deleteFn) (interface{}, error) {
	//TODO - create reverse deletion manager
	return nil, nil
}

func (dm *dbmanager) UpdateMultiDBRecordsTransaction(objs []UpdateTransactionObj, fns ...updateFn) (interface{}, error) {
	//TODO - create reverse updating manager
	return nil, nil
}

func (dm *dbmanager) RecordsTransaction(obj TransactionObj) (interface{}, error) {
	return dm.db.Transaction(func(session *xorm.Session) (interface{}, error) {
		switch obj.Action {
		case Create:
			if _, err := session.Insert(obj.Bean...); err != nil {
				return nil, err
			}
			fmt.Println(*&obj.Conditions[0])
			fmt.Println(&obj.Conditions[0])
			fmt.Println(*(&obj.Conditions[0]))
			fmt.Println(reflect.Indirect(reflect.ValueOf(obj.Conditions[0])))
			_, err := session.Insert(obj.Has...)
			if err != nil {
				return nil, err
			}
		}
		return nil, nil
	})
}

func (dm *dbmanager) ConvertRFC3339ToDate(datetime string) (sql.NullString, error) {
	const (
		layoutISO = "2006-01-02"
	)

	tmpDate := newNullString(datetime)

	if !tmpDate.Valid {
		return sql.NullString{}, nil
	} else {
		if parseTime, err := time.Parse(time.RFC3339, tmpDate.String); err != nil {
			return sql.NullString{}, err
		} else {
			return sql.NullString{
				Valid:  true,
				String: parseTime.Format(layoutISO),
			}, nil
		}
	}
}

func (dm *dbmanager) Close() error {
	if dm.db == nil && dm.srvCloser == nil {
		return nil
	}

	if dm.db != nil {
		if err := dm.db.Close(); err != nil {
			return err
		}
	}

	if dm.srvCloser != nil {
		log.Println("close server listening port")
		if err := dm.srvCloser.Close(); err != nil {
			return err
		}
	}
	return nil
}

func newNullString(s string) sql.NullString {
	if len(s) == 0 {
		return sql.NullString{}
	}
	return sql.NullString{
		String: s,
		Valid: true,
	}
}