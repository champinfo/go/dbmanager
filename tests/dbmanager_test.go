package tests

import (
	"errors"
	"fmt"
	databases "git.championtek.com.tw/go/dbmanager"
	"git.championtek.com.tw/go/dbmanager/database/members"
	"log"
	"testing"
	"time"
)

//test for migration
type Members struct {
	ID        string    `xorm:"not null pk CHAR(36) 'id'" json:"id"`
	CreatedAt time.Time `xorm:"TIMESTAMP created_at" json:"created_at"`
	UpdatedAt time.Time `xorm:"TIMESTAMP updated_at" json:"updated_at"`
}

type Profiles struct {
	ID        string    `xorm:"not null pk 'id' comment('個人檔案編號') CHAR(36)" json:"id"`
	MemberID  string    `xorm:"not null 'member_id' comment('會員編號') CHAR(36)" json:"memberId"`
	Birthday  time.Time `xorm:"comment('生日') DATE" json:"birthday"`
	Weight    string    `xorm:"default 0.0 comment('體重') DECIMAL(4,1)" json:"weight"`
	Height    string    `xorm:"default 0.0 comment('身高') DECIMAL(4,1)" json:"height"`
	Gender    int       `xorm:"not null comment('性別') TINYINT(1)" json:"gender"`
	Activity  int       `xorm:"not null comment('活動量') TINYINT(1)" json:"activity"`
	CreatedAt time.Time `xorm:"TIMESTAMP created" json:"-"`
	UpdatedAt time.Time `xorm:"TIMESTAMP updated" json:"-"`
}

func getConfig() databases.Config {
	dbConfig := databases.Config{
		Driver:             "mysql",
		DBName:             "members",
		UserName:           "champ",
		Password:           "work@t/6qup3",
		Host:               []string{"52.196.34.12"},
		Port:               "3306",
		MaxIdleConnections: 1024,
		MaxOpenConnections: 512,
		MaxLifeTime:        0,
		MonitorStatusPort:  0,
		IsDebug:            true,
	}
	return dbConfig
}

func TestConnection(t *testing.T) {
	dbConfig := getConfig()
	err := databases.Mgr.Init(&dbConfig)
	if err != nil {
		t.Error(err)
	}

	if err = databases.Mgr.Close(); err != nil {
		t.Error(err)
	}
}

func TestConvertToDate(t *testing.T) {
	//test empty
	if date, err := databases.Mgr.ConvertRFC3339ToDate(""); err != nil {
		t.Error(err)
	} else {
		if !date.Valid && len(date.String) != 0 {
			t.Error(errors.New("return sql.NullString is invalid"))
		} else if len(date.String) != 10 {
			t.Error(errors.New("return string is not date format"))
		} else {
			fmt.Println(date.String)
		}
	}

	if date, err := databases.Mgr.ConvertRFC3339ToDate("2020-10-22T16:33:45.96044902+08:00"); err != nil {
		t.Error(err)
	} else {
		if !date.Valid && len(date.String) != 0 {
			t.Error(errors.New("return sql.NullString is invalid"))
		} else if len(date.String) != 10 {
			t.Error(errors.New("return string is not date format"))
		} else {
			fmt.Println(date.String)
		}
	}
}

func TestGet(t *testing.T) {
	dbConfig := getConfig()
	err := databases.Mgr.Init(&dbConfig)
	if err != nil {
		t.Error(err)
	}
	var member = Members{}
	if _, err := databases.Mgr.GetRecord(&member); err != nil {
		t.Error(err)
	}

	if err = databases.Mgr.Close(); err != nil {
		t.Error(err)
	}
}

func TestFind(t *testing.T) {
	dbConfig := getConfig()
	err := databases.Mgr.Init(&dbConfig)
	if err != nil {
		t.Error(err)
	}

	var members []Members
	//member := Members{ID:"12976049-28ac-42dd-a5d7-792c01f9826a"}
	if err := databases.Mgr.FindRecords(&members, 0, 0, nil); err != nil {
		t.Error(err)
	}
	if err = databases.Mgr.Close(); err != nil {
		t.Error(err)
	}
}

func TestFindAndCount(t *testing.T) {
	dbConfig := getConfig()
	err := databases.Mgr.Init(&dbConfig)
	if err != nil {
		t.Error(err)
	}

	var members []Members

	counts, err := databases.Mgr.FindAndCount(&members)
	if err != nil {
		t.Error(err)
	}
	log.Println("counts - ", counts)

	if err = databases.Mgr.Close(); err != nil {
		t.Error(err)
	}
}

func TestInsertOrder(t *testing.T) {
	dbConfig := getConfig()
	err := databases.Mgr.Init(&dbConfig)
	if err != nil {
		t.Error(err)
	}
	var order = members.Orders{
		Id: "7ea548be-2afc-4634-a53f-56fda19d80fb",
		OrderNumber: "DE2ODR012020092100079",
		QuotationNo: "NU2QUO012020092100001",
		Expired: time.Now(),
		FreeMonths: 1,
		ActivationCode: "18JE2L",
		ProductionDescription: "",
		ProductCode: "1",
		Months: 1,
		PaymentType: 1,
		Amount: 6000,
		Status: 999,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	if _, err := databases.Mgr.CreateRecords(&order); err != nil {
		t.Error(err)
	}
}

//
//func TestFindLike(t *testing.T) {
//	dbConfig := getConfig()
//
//	err := databases.Mgr.Init(&dbConfig)
//	if err != nil {
//		t.Error(err)
//	}
//
//	var dishes []restaurants.Dishes
//
//	if err := databases.Mgr.FindLikeRecords(&dishes, 0, 0, "brand_id = ? AND name LIKE ?", "1", "%茶%"); err != nil {
//		t.Error(err)
//	}
//
//	if err := databases.Mgr.FindLikeRecords(&dishes, 5, 1, "brand_id = ? AND name LIKE ?", "1", "%茶%"); err != nil {
//		t.Error(err)
//	}
//
//	if err = databases.Mgr.Close(); err != nil {
//		t.Error(err)
//	}
//}
//
//func TestPagination(t *testing.T)  {
//	dbConfig := getConfig()
//
//	err := databases.Mgr.Init(&dbConfig)
//	if err != nil {
//		t.Error(err)
//	}
//
//	var dishes []restaurants.Dishes
//	if err := databases.Mgr.FindRecords(&dishes, 5, 3, &restaurants.Dishes{BrandId:1}); err != nil {
//		t.Error(err)
//	}
//
//	if err = databases.Mgr.Close(); err != nil {
//		t.Error(err)
//	}
//}
//
//func TestJoin(t *testing.T) {
//	dbConfig := getConfig()
//
//	err := databases.Mgr.Init(&dbConfig)
//	if err != nil {
//		t.Error(err)
//	}
//
//	type DishesBrand struct {
//		restaurants.Dishes `xorm:"extends"`
//		restaurants.Brands `xorm:"extends"`
//	}
//
//	var dishes restaurants.Dishes
//	var dishesBrand []DishesBrand
//	err = databases.Mgr.FindJoinRecords(&dishesBrand, &dishes, 0, 0, "INNER", "brands", "dishes.brand_id = brands.id")
//	if err != nil {
//		t.Error(err)
//	}
//}

//func TestInsertTransaction(t *testing.T) {
//	dbConfig := getConfig()
//
//	if err := databases.Mgr.Init(&dbConfig); err != nil {
//		t.Error(err)
//	}
//
//	diaryRecords := members.DiaryRecords{
//		MealDiaryId:  7,
//		MealTypeId:   3,
//		Image:        "1234567.jpg",
//		Carbohydrate: "6.700",
//		Protein:      "1.200",
//		Fats:         "2.000",
//		LocalPath:    "file://123456789/",
//		CreatedAt:    time.Now(),
//		UpdatedAt:    time.Now(),
//	}
//
//	diaryItem := members.DiaryItem{
//		DishId:         "21c05c37-e098-49cb-b682-0c517c45dac1",
//		UnitQuantities: "1.00",
//		X:              "0",
//		Y:              "0",
//		Height:         "0",
//		Width:          "0",
//		Confidence:     "0",
//		Label:          "沙拉",
//		Source:         1,
//		CreatedAt:      time.Now(),
//		UpdatedAt:      time.Now(),
//	}
//
//	diaryWater := members.DiaryWater{
//		MealDiaryId: 7,
//		Water:       1000,
//		CreatedAt:   time.Now(),
//		UpdatedAt:   time.Now(),
//	}
//
//	//	var beans = make([]interface{}, 0)
//	//	var conditions = make([]interface{}, 0)
//	//	var has = make([]interface{}, 0)
//	//
//	//	beans = append(beans, &diaryRecords)
//	//	has = append(has, &diaryItem, &diaryWater)
//	//	conditions = append(conditions, &diaryRecords.Id)
//	//
//	//	var dataObj = databases.TransactionObj{
//	//		Bean:       beans,
//	//		Conditions: conditions,
//	//		Action:     databases.Create,
//	//		Has:        has,
//	//	}
//	if _, err := databases.Mgr.InsertRecordsTransaction(diaryRecords, diaryItem, diaryWater); err != nil {
//		t.Error(err)
//	}
//	//	_, err := databases.Mgr.RecordsTransaction(dataObj)
//	//	fmt.Println("record id is ", diaryRecords.Id)
//	//	if err != nil {
//	//		t.Error(err)
//	//	}
//}

//
//func TestInsertTransactionRollback(t *testing.T) {
//
//}
//
//func TestUpdateTransaction(t *testing.T) {
//
//}
//
//func TestUpdateTransactionRollback(t *testing.T) {
//
//}
//
//func TestDeleteTransaction(t *testing.T) {
//
//}
//
//func TestDeleteTransactionRollback(t *testing.T) {
//
//}
