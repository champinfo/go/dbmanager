package databases

type TransactionObj struct {
	Bean       []interface{}
	Conditions []interface{}
	Action     int
	Has        []interface{} //you can use this to chain all related database action
}

type InsertTransactionObj struct {
	Bean     interface{}
	Cascades []interface{}
}

type DeleteTransactionObj struct {
	Bean     interface{}
	Cascades []interface{}
}

type UpdateTransactionObj struct {
	Bean             interface{}
	ConditionBean    interface{}
	ConditionCascade interface{}
	Cascades         []interface{}
}

const (
	Create = iota
	Update
	Delete
)
