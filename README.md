# dbmanager

## 負責管理資料庫連線

採用go語言撰寫，為確保連線的安全性，資料庫的登入帳號以及密碼採用讀取.env檔案方式；當自動部署時再將.env檔案內容注入docker．

所以會將local端的.env檔案加入gitignore，不會上git repo．
