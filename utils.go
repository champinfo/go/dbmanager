package databases

import (
	"database/sql/driver"
	"errors"
	"fmt"
)

type NullString string

func (s *NullString) Scan(value interface{}) error {
	fmt.Printf("scan %#v\n", value)
	if value == nil {
		*s = ""
		return nil
	}
	strVal, ok := value.(string)
	if !ok {
		return errors.New("column is not a string")
	}
	*s = NullString(strVal)
	return nil
}

func (s NullString) Value() (driver.Value, error)  {
	fmt.Printf("got value %#v\n", string(s))
	if len(s) == 0 { // if nil or empty string
		return nil, nil
	}
	return string(s), nil
}